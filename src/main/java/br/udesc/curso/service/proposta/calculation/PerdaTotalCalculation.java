package br.udesc.curso.service.proposta.calculation;

import java.util.Date;

import org.springframework.stereotype.Component;

import br.udesc.curso.vo.PropostaVo;

@Component
public class PerdaTotalCalculation extends CoberturaCalculation{

	private int idade(Date nas){
		Date today = new Date();
		return nas.getYear() - today.getYear(); 
	}
	
	@Override
	float calcularValor(PropostaVo proposta) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	float calcularPercentualVeiculo(PropostaVo proposta) {
		int idade = idade(proposta.getCliente().getNascimento());
		if (idade < 40) {
			if(idade < 24) {
				return (float) 0.05;
			} else {
				return (float) 0.03;
			}
		} else {			
			return (float) 0.01;
		}
	}
	
}
