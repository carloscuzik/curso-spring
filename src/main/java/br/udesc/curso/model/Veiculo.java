package br.udesc.curso.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class Veiculo {
	
	@Column(length=8, updatable=false, nullable=false, unique=true)
	private String placa;
	
	private Date anoDeFabricacao;

	private float valor;
	
	private boolean blindado;
	
	private boolean importado;
}
