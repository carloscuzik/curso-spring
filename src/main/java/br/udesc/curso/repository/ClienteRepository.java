package br.udesc.curso.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.udesc.curso.model.Cliente;

public interface ClienteRepository
	extends JpaRepository<Cliente, Long>{
	
}
