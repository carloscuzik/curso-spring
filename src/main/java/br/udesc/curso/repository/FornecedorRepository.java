package br.udesc.curso.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.udesc.curso.model.Fornecedor;

public interface FornecedorRepository
	extends JpaRepository<Fornecedor, Long>{
	
}
