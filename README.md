# `Spring` `Boot` `Tutorial`

## Install dependences

- clone this repository

- open your `IDE` (I using Eclipse) "(>_<)"

- import this folder

- Remember to look you `applications.properties` file and write
```properties
server.port = 8081
```

### Install Lombok

```link
https://projectlombok.org/setup/eclipse
```

## Using

- run `TesteApplication.java`
- access root <<<not work>>>
```link
http://localhost:8081/
```


## Important things

link to test REST API

```
https://pastebin.com/Jmj8TvkQ
```

### DB (H2) Edition

- access

```link
http://localhost:8081/h2
```

- swagger

```
http://localhost:8081/swagger-ui.html
```

- log-in with

```text
jdbc:h2:file:~/curso
```






I really like `Rails` and `Django`